import cv2
import numpy as np

from math import pi
from math import sin
from math import cos


# Load an color image in color
img = cv2.imread('data/lines/fasad.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 50, 150, apertureSize = 3)

lines = cv2.HoughLines(edges, 1, pi / 180, 10)
lines_image = np.zeros(img.shape)
for line in lines:
    rho = line[0][0]
    theta = line[0][1]
    a = cos(theta)
    b = sin(theta)
    x0 = a * rho
    y0 = b * rho

    x1, y1 = int(x0 + 1000 * (-b)), int(y0 + 1000 * a)
    x2, y2 = int(x0 - 1000 * (-b)), int(y0 - 1000 * a)

    cv2.line(img=lines_image, pt1=(x1, y1), pt2=(x2, y2), color=(0, 255, 0), thickness=3)
cv2.imshow('Hough', lines_image)

cv2.waitKey(0)
cv2.destroyAllWindows()




