import cv2

# Load an color image in color
img = cv2.imread('data/circles/light.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 2, gray.shape[1] / 4, param1=100, param2=30, minRadius=20, maxRadius=0)
for circle in circles[0, :]:
    x, y, r = int(circle[0]), int(circle[1]), int(circle[2])
    cv2.circle(img=img, center=(x, y), radius=r, color=(0, 0, 0), thickness=2)

cv2.imshow('Hough circles', img)

cv2.waitKey(0)
cv2.destroyAllWindows()




