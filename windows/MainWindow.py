from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtWidgets import QGraphicsPixmapItem
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QDir
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QImage

import cv2
import os

# UI
from windows.uis.mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):
    """
    Class implementing PyQt5 window with

    - multiple widgets (checkboxes, pushbuttons, lineedits, sliders, comboboxes)

    - simple cv2 wrapper with QGraphicsView imaging, mouse wheel zoom, open and save buttons

    - b'n'w checkbox

    - subtask choose combobox

    Attributes:

        new_image   The pyQtSignal to call when self.image been updated. |
        MODES   The constant int enumeration mode.
    """
    class MODES:
        BLACK_N_WHITE = 0
        COLORFUL = 1
    new_image = pyqtSignal()

    def __init__(self, directory=r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\data"):
        """
        Construct new instance of Task.
        :param directory: str -- current working directory, being opened as default in save / open dialogs.
        """
        QMainWindow.__init__(self)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.setWindowTitle("Обработка")
        icon = QIcon("icon.ico")
        self.setWindowIcon(icon)

        # Fix window size
        self.setFixedSize(self.geometry().width(), self.geometry().height())

        self.scene = QGraphicsScene()
        self.ui.graphicsView.setScene(self.scene)

        self.zoom = 0
        self.mode = self.MODES.COLORFUL
        self.file_name = None

        self.original = None
        self.edited = None
        self.image = None

        self.directory = directory

        self.ui.openPB.clicked.connect(self.open_image)
        self.ui.savePB.clicked.connect(self.save_image)
        self.ui.isBlackAndWhiteCB.stateChanged.connect(self.toggle_bw)

        # Set up subtask combo
        self.ui.subtaskCB.addItem("Original", QVariant("Original"))
        self.ui.subtaskCB.activated.connect(lambda idx: self.execute())

        self.ui.graphicsView.installEventFilter(self)

        self.new_image.connect(self.execute)

    def reopen_image(self):
        """
        Reopen image with the same filename.
        :return: None
        """
        if self.file_name is None:
            return
        self.original = cv2.imread(self.file_name, self.mode)
        self.new_image.emit()

    def open_image(self):
        """
        Open new image with QFileDialog. Connected to open pushbutton.
        :return: None
        """
        file_name = QFileDialog.getOpenFileName(
            parent=self,
            caption="Open image file",
            directory=self.directory,
            filter="Images (*.png *.xpm *.jpg)")[0]

        # Kicks out case of Cancel in file dialog
        if file_name == "":
            return

        # Get relative path for cv
        directory = QDir(os.getcwd())
        self.file_name = directory.relativeFilePath(file_name)

        self.original = cv2.imread(self.file_name, self.mode)
        self.new_image.emit()

    # def toggle_original(self, is_original):
    #     if is_original:
    #         self.update_image(self.original)
    #     else:
    #         self.update_image(self.edited)

    def save_image(self):
        """
        Save edited image with QFileDialog. Connected to save pushbutton.
        :return: None
        """
        # If no image loaded
        if self.image is None:
            return

        file_name = QFileDialog.getSaveFileName(
            parent=self,
            caption="Open image file",
            directory=self.directory,
            filter="Image (*.jpg)")[0]

        # Kicks out case of Cancel in file dialog
        if file_name == "":
            return

        # Get relative path for cv
        directory = QDir(os.getcwd())
        self.file_name = directory.relativeFilePath(file_name)

        # Save
        self.original = cv2.imwrite(self.file_name, self.image)

    def toggle_bw(self, is_bw: bool) -> None:
        """
        Set image b'n'w or colored.
        :param is_bw: bool -- boolean determining if the image should be b'n'w or colorful.
        :return: None
        """
        if is_bw:
            self.mode = self.MODES.BLACK_N_WHITE
        else:
            self.mode = self.MODES.COLORFUL
        # TODO: remove reopening file on change black'n'white mode -- just convert original image to gray if necessary
        self.reopen_image()

    def update_image(self, image):
        """
        Show new image in QGraphicsView.
        :param image: np.array -- new cv2 image to show.
        :return: None
        """
        # TODO: remove
        if image is None:
            return

        # Convert
        self.image = image

        # If grayscaled -- convert to colored
        if len(self.image.shape) == 2:
            self.image = cv2.cvtColor(self.image, cv2.COLOR_GRAY2RGB)

        # Create QImage to load to scene
        height, width, byte_value = self.image.shape
        byte_value *= width
        qimage = QImage(self.image, width, height, byte_value, QImage.Format_RGB888)

        # Convert rgb to bgr to show with Qt
        qimage =QImage.rgbSwapped(qimage)

        # Load QImage to scene
        item = QGraphicsPixmapItem(QPixmap.fromImage(qimage))
        self.scene.clear()
        self.scene.addItem(item)

        # Show new scene
        self.ui.graphicsView.show()

    def wheelEvent(self, event):
        """
        Handle mouse wheel event to zoom QGraphicsView.
        :param event: PyQy5 mouse wheel event -- event to handle.
        :return: None
        """
        old_pos = self.ui.graphicsView.mapToScene(event.pos())

        factor = 1.1 if event.angleDelta().y() > 0 else 0.9
        self.ui.graphicsView.scale(factor, factor)

        new_pos = self.ui.graphicsView.mapToScene(event.pos())
        delta = new_pos - old_pos

        self.ui.graphicsView.translate(delta.x(), delta.y())

        event.accept()

    def execute(self):
        """
        Execute task. Must be overrode with inheritance in new task.
        :return: None
        """
        pass
