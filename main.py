# -*- coding: utf-8 -*-

import sys
import traceback
from PyQt5.QtWidgets import QApplication

from tasks.Task5 import Task5MainWindow
from tasks.Task6 import Task6MainWindow
from tasks.Task7 import Task7MainWindow
from tasks.Task8 import Task8MainWindow
from tasks.Task9 import Task9MainWindow


if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        window = Task9MainWindow()
        window.show()
        sys.exit(app.exec_())
    except Exception as e:
        traceback.print_exc()
