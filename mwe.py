import cv2

# Load an color image in color
img = cv2.imread('data/1.jpg', 1)

# Show image
cv2.imshow('after', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
