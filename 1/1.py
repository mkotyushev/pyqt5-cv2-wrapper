import cv2


def number_brighter(image, trashold):
    """
    :param image: numpy.array -- grayscaled image
    :param trashold: int -- trashold value in range 0 : 255
    :return: number: int -- number of pixels with brightness more then trashold
    """
    number = 0
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i][j] >= trashold:
                number += 1
    return number


# Load an color image in color
img = cv2.imread('data/1.jpg', 1)
cv2.imshow('before', img)

# Get grayscaled image
img = cv2.cvtColor(img, 7)

# Calculate amount of pixels that brighter then T
T = 4
number = number_brighter(img, T)
print(float(number) / (img.shape[0] * img.shape[1]))

# Show image
cv2.imshow('after', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
