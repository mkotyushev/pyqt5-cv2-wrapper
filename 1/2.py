import cv2


# mouse callback function
def draw(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(img, (x, y), 10, (255, 0, 0), -1)
    if event == cv2.EVENT_RBUTTONDBLCLK:
        cv2.rectangle(img, (x - 10, y - 10), (x + 10, y + 10), (0, 0, 255), -1)
    if event == cv2.EVENT_MBUTTONDBLCLK:
        cv2.ellipse(img, (x - 10, y - 20), (10, 20), 0, 0, 360, 50, -1)

# Load an color image in color
img = cv2.imread("data/1.jpg", 1)

# Bind handler for mouse events
cv2.namedWindow("image")
cv2.setMouseCallback("image", draw)

cv2.circle(img, (210, 175), 20, (255, 0, 0), 5)

while 1:
    cv2.imshow("image", img)
    if cv2.waitKey(20) & 0xFF == 27:
        break
cv2.destroyAllWindows()

cv2.imwrite("data/out/1.jpg", img)
