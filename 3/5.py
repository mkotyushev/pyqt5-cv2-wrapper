import cv2
import numpy as np
import copy

names = ['light', 'middle', 'hard']
for name in names:
    # Load an color image in color
    img = cv2.imread('data/' + name + '.jpg', 0)
    # Show image
    cv2.imshow('orig_' + name, img)

    # Create copy and apply
    filtered_img = copy.copy(img)
    cv2.threshold(img, 255, 255, cv2.THRESH_BINARY, filtered_img)
    # Show image
    cv2.imshow('after_' + name, filtered_img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
