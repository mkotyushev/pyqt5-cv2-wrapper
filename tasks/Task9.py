import cv2
import copy
from itertools import cycle

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant


def get_colors(c):
    result = []
    i, j, k = 0, 0, 0
    for n in range(48):
        r, g, b = 0, 0, 0
        if n % 3 == 0:
            r = (i + 1) * 16 - 1 + c
            i += 1
        if n % 3 == 1:
            g = (j + 1) * 16 - 1 + c
            j += 1
        if n % 3 == 2:
            b = (j + 1) * 16 - 1 + c
            j += 1
        result.append((r, g, b))
    return result


class Task9MainWindow(MainWindow):
    COLORS = get_colors(100)
    THICKNESSES = [
        2
    ]

    def __init__(self):
        super().__init__(directory=r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\5\data")

        # Add subtasks
        self.ui.subtaskCB.addItem("Threshold", QVariant("Threshold"))
        self.ui.subtaskCB.addItem("Moments", QVariant("Moments"))
        self.ui.subtaskCB.addItem("Hu", QVariant("Hu"))
        self.ui.subtaskCB.addItem("Match", QVariant("Match"))

        # Enable slider
        self.ui.horizontalSliderLabel.setText("Threshold")
        self.ui.horizontalSlider.setMinimum(0)
        self.ui.horizontalSlider.setMaximum(254)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)

        # Enable slider 2
        self.ui.horizontalSlider_2Label.setText("Count num")
        self.ui.horizontalSlider_2.setMinimum(0)
        self.ui.horizontalSlider_2.setValue(0)
        self.ui.horizontalSlider_2.setMaximum(100)
        self.ui.horizontalSlider_2.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_2.setEnabled(True)

        # Enable slider 3
        self.ui.horizontalSlider_3Label.setText("Max matches")
        self.ui.horizontalSlider_3.setMinimum(1)
        self.ui.horizontalSlider_3.setValue(1)
        self.ui.horizontalSlider_3.setMaximum(10)
        self.ui.horizontalSlider_3.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_3.setEnabled(True)

        # Enable combo for Mode
        self.ui.comboBoxLabel.setText("Count modes")
        self.ui.comboBox.addItem("cv2.RETR_EXTERNAL", QVariant(cv2.RETR_EXTERNAL))
        self.ui.comboBox.addItem("cv2.RETR_LIST", QVariant(cv2.RETR_LIST))
        self.ui.comboBox.addItem("cv2.RETR_CCOMP", QVariant(cv2.RETR_CCOMP))
        self.ui.comboBox.addItem("cv2.RETR_TREE", QVariant(cv2.RETR_TREE))
        # self.ui.comboBox.addItem("cv2.RETR_FLOODFILL", QVariant(cv2.RETR_FLOODFILL))
        self.ui.comboBox.activated.connect(lambda idx: self.execute())
        self.ui.comboBox.setEnabled(True)

        # Enable combo for App type
        self.ui.comboBox_2Label.setText("Count app type")
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_NONE", QVariant(cv2.CHAIN_APPROX_NONE))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_SIMPLE", QVariant(cv2.CHAIN_APPROX_SIMPLE))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_TC89_L1", QVariant(cv2.CHAIN_APPROX_TC89_L1))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_TC89_KCOS", QVariant(cv2.CHAIN_APPROX_TC89_KCOS))
        self.ui.comboBox_2.activated.connect(lambda idx: self.execute())
        self.ui.comboBox_2.setEnabled(True)

        # Enable checkbox for manual / automotive threshold
        self.ui.checkBox.setText("Draw contours")
        self.ui.checkBox.setChecked(True)
        self.ui.checkBox.stateChanged.connect(self.execute)
        self.ui.checkBox.setEnabled(True)

        # Enable combo for manual type
        self.ui.comboBox_3Label.setText("Tr mode")
        self.ui.comboBox_3.addItem("cv2.THRESH_BINARY", QVariant(cv2.THRESH_BINARY))
        self.ui.comboBox_3.addItem("cv2.THRESH_BINARY_INV", QVariant(cv2.THRESH_BINARY_INV))
        # self.ui.comboBox_3.addItem("cv2.THRESH_TRUNC", QVariant(cv2.THRESH_TRUNC))
        self.ui.comboBox_3.addItem("cv2.THRESH_TOZERO", QVariant(cv2.THRESH_TOZERO))
        self.ui.comboBox_3.addItem("cv2.THRESH_TOZERO_INV", QVariant(cv2.THRESH_TOZERO_INV))
        self.ui.comboBox_3.addItem("cv2.THRESH_BINARY + cv2.THRESH_OTSU", QVariant(cv2.THRESH_BINARY + cv2.THRESH_OTSU))
        self.ui.comboBox_3.activated.connect(lambda idx: self.execute())
        self.ui.comboBox_3.setEnabled(True)

        # Enable combo for matching method
        self.ui.comboBox_4Label.setText("Match type")
        self.ui.comboBox_4.addItem("cv2.CONTOURS_MATCH_I1", QVariant(cv2.CONTOURS_MATCH_I1))
        self.ui.comboBox_4.addItem("cv2.CONTOURS_MATCH_I2", QVariant(cv2.CONTOURS_MATCH_I2))
        self.ui.comboBox_4.addItem("cv2.CONTOURS_MATCH_I3", QVariant(cv2.CONTOURS_MATCH_I3))
        self.ui.comboBox_4.activated.connect(lambda idx: self.execute())
        self.ui.comboBox_4.setEnabled(True)

    def execute(self):
        # Check if image is been loaded
        if self.original is None:
            return

        # Copy original image into edited instance
        self.edited = copy.copy(self.original)

        # Create references to increase readability
        image = self.original
        result = self.edited

        # Get params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        # Get other params from ui
        threshold = self.ui.horizontalSlider.value()
        threshold_mode = self.ui.comboBox_3.itemData(self.ui.comboBox_3.currentIndex())
        mode = self.ui.comboBox.itemData(self.ui.comboBox.currentIndex())
        app_type = self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex())
        draw_contours = self.ui.checkBox.isChecked()
        match_mode = self.ui.comboBox_4.itemData(self.ui.comboBox_4.currentIndex())
        contour_num = self.ui.horizontalSlider_2.value()
        max_matches = self.ui.horizontalSlider_3.value()

        if is_black_and_white:
            # Set output to colored
            result = cv2.cvtColor(result, cv2.COLOR_GRAY2RGB)

            if subtask == "Original":
                pass
            else:
                # Common code for all subtasks
                ret, thresh = cv2.threshold(image, threshold, 255, threshold_mode)
                im2, contours, hierarchy = cv2.findContours(thresh, mode, app_type)

                cnt = contours[0]
                M = cv2.moments(cnt)

                if subtask == "Threshold":
                    result = thresh
                elif subtask == "Moments":
                    print(M)
                elif subtask == "Hu":
                    hu_M = cv2.HuMoments(M)
                    print(hu_M)
                elif subtask == "Match":
                    if draw_contours:
                        # Get metric -- match lvl between 2 contours
                        matches = []
                        for contour1 in contours:
                            matches_entry = []
                            for contour2 in contours:
                                if contour1 is not contour2:
                                    match_metric = cv2.matchShapes(contour1, contour2, match_mode, 0.0)
                                    matches_entry.append((match_metric, contour1, contour2))
                            # Sort pairs of contours by metric
                            matches_entry = sorted(matches_entry, key=lambda item: item[0])
                            matches.append(matches_entry)
                        # Draw best matching contour paris with same color
                        color_iter = cycle(self.COLORS)
                        thickness_iter = cycle(self.THICKNESSES)

                        color, thickness = next(color_iter), next(thickness_iter)
                        matches_entry = matches[contour_num % len(matches)]
                        i = 0
                        for metric, contour1, contour2 in matches_entry:
                            if i >= max_matches:
                                break
                            cv2.drawContours(result, [contour1], 0, color, thickness)
                            cv2.drawContours(result, [contour2], 0, color, thickness)
                            i += 1
        else:
            pass

        self.update_image(result)
