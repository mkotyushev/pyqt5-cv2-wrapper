import cv2
import copy

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant


class Task6MainWindow(MainWindow):
    def __init__(self):
        super().__init__()

        self.directory = r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\3\data"

        self.ui.subtaskCB.addItem("Кэнни", QVariant("Кэнни"))
        self.ui.subtaskCB.addItem("Сумма", QVariant("Сумма"))
        self.ui.subtaskCB.addItem("Контуры", QVariant("Контуры"))
        self.ui.subtaskCB.addItem("Прямоугольники", QVariant("Прямоугольники"))
        
        # Enable slider for manual threshold
        self.ui.horizontalSliderLabel.setText("Threshold 1")
        self.ui.horizontalSlider.setMinimum(0)
        self.ui.horizontalSlider.setMaximum(255)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)
        
        # Enable slider for manual threshold
        self.ui.horizontalSlider_2Label.setText("Threshold 2")
        self.ui.horizontalSlider_2.setMinimum(0)
        self.ui.horizontalSlider_2.setMaximum(255)
        self.ui.horizontalSlider_2.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_2.setEnabled(True)

        # Enable combo for Mode
        self.ui.comboBoxLabel.setText("Modes")
        self.ui.comboBox.addItem("cv2.RETR_EXTERNAL", QVariant(cv2.RETR_EXTERNAL))
        self.ui.comboBox.addItem("cv2.RETR_LIST", QVariant(cv2.RETR_LIST))
        self.ui.comboBox.addItem("cv2.RETR_CCOMP", QVariant(cv2.RETR_CCOMP))
        self.ui.comboBox.addItem("cv2.RETR_TREE", QVariant(cv2.RETR_TREE))
        self.ui.comboBox.addItem("cv2.RETR_FLOODFILL", QVariant(cv2.RETR_FLOODFILL))
        self.ui.comboBox.activated.connect(lambda idx: self.execute())
        self.ui.comboBox.setEnabled(True)

        # Enable combo for App type
        self.ui.comboBox_2Label.setText("App type")
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_NONE", QVariant(cv2.CHAIN_APPROX_NONE))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_SIMPLE", QVariant(cv2.CHAIN_APPROX_SIMPLE))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_TC89_L1", QVariant(cv2.CHAIN_APPROX_TC89_L1))
        self.ui.comboBox_2.addItem("cv2.CHAIN_APPROX_TC89_KCOS", QVariant(cv2.CHAIN_APPROX_TC89_KCOS))
        self.ui.comboBox_2.activated.connect(lambda idx: self.execute())
        self.ui.comboBox_2.setEnabled(True)

    def execute(self):
        if self.original is None:
            return
        self.edited = copy.copy(self.original)

        image = self.original
        result = self.edited

        # Get common params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        # Get other params from ui
        treshold1 = self.ui.horizontalSlider.value()
        treshold2 = self.ui.horizontalSlider_2.value()
        mode = self.ui.comboBox.itemData(self.ui.comboBox.currentIndex())
        app_type = self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex())

        if is_black_and_white:
            if subtask == "Оригинал":
                pass
            else:
                canny = copy.copy(image)
                cv2.Canny(image, treshold1, treshold2, canny, L2gradient=True)
                if subtask == "Кэнни":
                    result = canny
                elif subtask == "Сумма":
                    result = cv2.add(canny, image)
                elif subtask in ["Контуры", "Прямоугольники"]:
                    im2, contours, hierarchy = cv2.findContours(canny, mode=mode, method=app_type)
                    if subtask == "Контуры":
                        cv2.drawContours(result, contours, -1, (0, 255, 0), 3)
                    elif subtask == "Прямоугольники":
                        for contour in contours:
                            x, y, w, h = cv2.boundingRect(contour)
                            cv2.rectangle(result, (x, y), (x + w, y + h), (0, 255, 0), 2)

        self.update_image(result)
