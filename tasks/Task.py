import cv2
import copy

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant


class TaskMainWindow(MainWindow):
    def __init__(self):
        super().__init__()

        self.directory = r"C:\Users\mkotyushev"

        # Add subtasks
        self.ui.subtaskCB.addItem("Sub 1", QVariant("Sub 1"))
        self.ui.subtaskCB.addItem("Sub 2", QVariant("Sub 2"))
        self.ui.subtaskCB.addItem("Sub 3", QVariant("Sub 3"))
        self.ui.subtaskCB.addItem("Sub 4", QVariant("Sub 4"))

        # Enable slider
        self.ui.horizontalSliderLabel.setText("Slider")
        self.ui.horizontalSlider.setMinimum(0)
        self.ui.horizontalSlider.setMaximum(255)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)

    def execute(self):
        # Check if image is been loaded
        if self.original is None:
            return

        # Copy original image into edited instance
        self.edited = copy.copy(self.original)

        # Create references to increase readability
        image = self.original
        result = self.edited

        # Get params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        if is_black_and_white:
            if subtask == "Original":
                pass
            else:
                # Common code for all subtasks
                if subtask == "Sub 1":
                    pass
                elif subtask == "Sub 2":
                    pass
                elif subtask in ["Sub 3", "Sub 4"]:
                    # Common code for 3 and 4 subtasks
                    if subtask == "Sub 3":
                        pass
                    elif subtask == "Sub 4":
                        pass
        else:
            pass

        self.update_image(result)
