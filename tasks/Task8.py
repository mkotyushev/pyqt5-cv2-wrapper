import cv2
import copy

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant


class Task8MainWindow(MainWindow):
    def __init__(self):
        super().__init__()

        # Set directory
        self.directory = r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\4\data"

        # Add subtasks
        self.ui.subtaskCB.addItem("HoughCircles", QVariant("HoughCircles"))

        # Enable slider for min dist
        self.ui.horizontalSliderLabel.setText("Min dist")
        self.ui.horizontalSlider.setMinimum(1)
        self.ui.horizontalSlider.setValue(50)
        self.ui.horizontalSlider.setMaximum(255)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)

        # Enable slider for min rad
        self.ui.horizontalSlider_2Label.setText("Min rad")
        self.ui.horizontalSlider_2.setMinimum(0)
        self.ui.horizontalSlider_2.setValue(20)
        self.ui.horizontalSlider_2.setMaximum(255)
        self.ui.horizontalSlider_2.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_2.setEnabled(True)

        # Enable slider for max rad
        self.ui.horizontalSlider_3Label.setText("Max rad")
        self.ui.horizontalSlider_3.setMinimum(0)
        self.ui.horizontalSlider_3.setValue(0)
        self.ui.horizontalSlider_3.setMaximum(1000)
        self.ui.horizontalSlider_3.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_3.setEnabled(True)

        # Enable slider for param1
        self.ui.horizontalSlider_4Label.setText("Param1")
        self.ui.horizontalSlider_4.setMinimum(1)
        self.ui.horizontalSlider_4.setValue(100)
        self.ui.horizontalSlider_4.setMaximum(500)
        self.ui.horizontalSlider_4.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_4.setEnabled(True)

        # Enable slider for param2
        self.ui.horizontalSlider_5Label.setText("Param2")
        self.ui.horizontalSlider_5.setMinimum(1)
        self.ui.horizontalSlider_5.setValue(30)
        self.ui.horizontalSlider_5.setMaximum(100)
        self.ui.horizontalSlider_5.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_5.setEnabled(True)

    def execute(self):
        if self.original is None:
            return
        self.edited = copy.copy(self.original)

        # Create references to increase readability
        image = self.original
        result = self.edited

        # Get common params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        # Get other params from ui
        min_dist = self.ui.horizontalSlider.value()
        min_radius = self.ui.horizontalSlider_2.value()
        max_radius = self.ui.horizontalSlider_3.value()
        param1 = self.ui.horizontalSlider_4.value()
        param2 = self.ui.horizontalSlider_5.value()
        thickness = 2

        if is_black_and_white:
            if subtask == "Original":
                pass
            else:
                result = cv2.cvtColor(result, cv2.COLOR_GRAY2RGB)
                # Code for subtasks
                if subtask == "HoughCircles":
                    circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1, min_dist, param1=param1, param2=param2, minRadius=min_radius, maxRadius=max_radius)
                    for circle in circles[0, :]:
                        x, y, r = int(circle[0]), int(circle[1]), int(circle[2])
                        cv2.circle(img=result, center=(x, y), radius=r, color=(0, 255, 0), thickness=thickness)
        else:
            pass

        self.update_image(result)
