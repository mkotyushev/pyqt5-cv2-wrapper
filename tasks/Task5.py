import cv2
import copy

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant


class Task5MainWindow(MainWindow):
    def __init__(self):
        super().__init__()

        self.directory = r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\3\data"
        self.ui.subtaskCB.addItem("Manual", QVariant("Manual"))

        # Enable slider for manual threshold
        self.ui.horizontalSliderLabel.setText("Threshold")
        self.ui.horizontalSlider.setMinimum(0)
        self.ui.horizontalSlider.setMaximum(255)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)

        # Enable slider for manual threshold
        self.ui.horizontalSlider_2Label.setText("Block size")
        self.ui.horizontalSlider_2.setMinimum(3)
        self.ui.horizontalSlider_2.setMaximum(45)
        self.ui.horizontalSlider_2.setSingleStep(2)
        self.ui.horizontalSlider_2.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_2.setEnabled(True)
        
        # Enable slider for manual threshold
        self.ui.horizontalSlider_3Label.setText("Const")
        self.ui.horizontalSlider_3.setMinimum(0)
        self.ui.horizontalSlider_3.setMaximum(255)
        self.ui.horizontalSlider_3.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_3.setEnabled(True)

        # Enable checkbox for manual / automotive threshold
        self.ui.checkBox.setText("Manual")
        self.ui.checkBox.setChecked(True)
        self.ui.checkBox.stateChanged.connect(self.execute)
        self.ui.checkBox.setEnabled(True)

        # Enable combo for manual type
        self.ui.comboBoxLabel.setText("THRESH")
        self.ui.comboBox.addItem("cv2.THRESH_BINARY", QVariant(cv2.THRESH_BINARY))
        self.ui.comboBox.addItem("cv2.THRESH_BINARY_INV", QVariant(cv2.THRESH_BINARY_INV))
        self.ui.comboBox.addItem("cv2.THRESH_TRUNC", QVariant(cv2.THRESH_TRUNC))
        self.ui.comboBox.addItem("cv2.THRESH_TOZERO", QVariant(cv2.THRESH_TOZERO))
        self.ui.comboBox.addItem("cv2.THRESH_TOZERO_INV", QVariant(cv2.THRESH_TOZERO_INV))
        self.ui.comboBox.addItem("cv2.THRESH_BINARY + cv2.THRESH_OTSU", QVariant(cv2.THRESH_BINARY + cv2.THRESH_OTSU))
        self.ui.comboBox.activated.connect(lambda idx: self.execute())
        self.ui.comboBox.setEnabled(True)

        # Enable combo for auto type
        self.ui.comboBox_2Label.setText("THRESH AUTO")
        self.ui.comboBox_2.addItem("cv2.ADAPTIVE_THRESH_MEAN_C", QVariant(cv2.ADAPTIVE_THRESH_MEAN_C))
        self.ui.comboBox_2.addItem("cv2.ADAPTIVE_THRESH_GAUSSIAN_C", QVariant(cv2.ADAPTIVE_THRESH_GAUSSIAN_C))
        self.ui.comboBox_2.activated.connect(lambda idx: self.execute())
        self.ui.comboBox_2.setEnabled(True)

    def execute(self):
        if self.original is None:
            return
        self.edited = copy.copy(self.original)

        # Create references to increase readability
        image = self.original
        result = self.edited

        # Get common params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        is_manual = self.ui.checkBox.isChecked()
        tresh_type = self.ui.comboBox.itemData(self.ui.comboBox.currentIndex())
        adap_type = self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex())
        threshold = self.ui.horizontalSlider.value()
        block_size = self.ui.horizontalSlider_2.value()
        const = self.ui.horizontalSlider_3.value()

        if subtask == "Original":
            pass
        elif subtask == "Manual":
            if is_manual:
                cv2.threshold(self.original, threshold, 255, tresh_type, self.edited)
            else:
                if is_black_and_white:
                    cv2.adaptiveThreshold(self.original, 255, adap_type, tresh_type, block_size, const, self.edited)

        self.update_image(self.edited)
