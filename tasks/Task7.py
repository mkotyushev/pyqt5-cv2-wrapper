import cv2
import copy

from windows.MainWindow import MainWindow

from PyQt5.QtCore import QVariant

from math import pi
from math import sin
from math import cos


class Task7MainWindow(MainWindow):
    def __init__(self):
        super().__init__()

        # Set directory
        self.directory = r"C:\Users\mkotyushev\Google Диск\4 курс\Обработка\Семинары\4\data"

        # Add subtasks
        self.ui.subtaskCB.addItem("Hough", QVariant("Hough"))
        self.ui.subtaskCB.addItem("HoughP", QVariant("HoughP"))

        # Enable slider for manual threshold
        self.ui.horizontalSliderLabel.setText("Canny threshold 1")
        self.ui.horizontalSlider.setMinimum(0)
        self.ui.horizontalSlider.setMaximum(255)
        self.ui.horizontalSlider.valueChanged.connect(self.execute)
        self.ui.horizontalSlider.setEnabled(True)

        # Enable slider for manual threshold
        self.ui.horizontalSlider_2Label.setText("Canny threshold 2")
        self.ui.horizontalSlider_2.setMinimum(0)
        self.ui.horizontalSlider_2.setMaximum(255)
        self.ui.horizontalSlider_2.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_2.setEnabled(True)
        
        # Enable slider for votes
        self.ui.horizontalSlider_3Label.setText("Votes")
        self.ui.horizontalSlider_3.setMinimum(0)
        self.ui.horizontalSlider_3.setMaximum(255)
        self.ui.horizontalSlider_3.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_3.setEnabled(True)

        # Enable slider for min len
        self.ui.horizontalSlider_4Label.setText("Min len")
        self.ui.horizontalSlider_4.setMinimum(0)
        self.ui.horizontalSlider_4.setMaximum(255)
        self.ui.horizontalSlider_4.valueChanged.connect(self.execute)
        self.ui.horizontalSlider_4.setEnabled(True)

    def execute(self):
        if self.original is None:
            return
        self.edited = copy.copy(self.original)

        # Create references to increase readability
        image = self.original
        result = self.edited

        # Get common params from ui
        subtask = self.ui.subtaskCB.itemData(self.ui.subtaskCB.currentIndex())
        is_black_and_white = self.ui.isBlackAndWhiteCB.isChecked()

        # Get other params from ui
        treshold1 = self.ui.horizontalSlider.value()
        treshold2 = self.ui.horizontalSlider_2.value()
        votes = self.ui.horizontalSlider_3.value()
        minLen = self.ui.horizontalSlider_4.value()
        maxGap = minLen / 2
        thickness = 2

        if is_black_and_white:
            if subtask == "Original":
                pass
            else:
                # Code for subtasks
                canny = copy.copy(image)
                cv2.Canny(image, treshold1, treshold2, canny)
                if subtask == "Hough":
                    lines = cv2.HoughLines(canny, 1, pi / 180, votes)
                    for line in lines:
                        rho = line[0][0]
                        theta = line[0][1]
                        a = cos(theta)
                        b = sin(theta)
                        x0 = a * rho
                        y0 = b * rho

                        x1, y1 = int(x0 + 1000 * (-b)), int(y0 + 1000 * a)
                        x2, y2 = int(x0 - 1000 * (-b)), int(y0 - 1000 * a)

                        cv2.line(img=result, pt1=(x1, y1), pt2=(x2, y2), color=(0, 0, 0), thickness=thickness)

                elif subtask == "HoughP":
                    lines = cv2.HoughLinesP(canny, 1, pi / 180, votes, minLen, maxGap)
                    for line in lines:
                        x1, y1 = line[0][0], line[0][1]
                        x2, y2 = line[0][2], line[0][3]

                        cv2.line(img=result, pt1=(x1, y1), pt2=(x2, y2), color=(0, 0, 0), thickness=thickness)

        else:
            pass

        self.update_image(result)
