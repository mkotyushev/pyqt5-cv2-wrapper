import cv2
from matplotlib import pyplot as plt


# Load an color image in color
img = cv2.imread('data/2.jpg', 0)

# Calculate histogram
hist = cv2.calcHist([img], [0], None, [256], [0, 256])
plt.plot(range(256), hist, 'r')

# Eq hist
eq_img = img
cv2.equalizeHist(img, eq_img)

# Calculate eq histogram
eq_hist = cv2.calcHist([eq_img], [0], None, [256], [0, 256])
plt.plot(range(256), eq_hist, 'b')

plt.show()

# Show images
cv2.imshow('image', img)
cv2.imshow('eq image', eq_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
