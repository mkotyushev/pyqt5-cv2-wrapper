import numpy as np
import cv2


def saturate_cast(number, low, high):
    if number > high:
        return high
    elif number < low:
        return low
    else:
        return number


def linear_correction(image, alpha, beta):
    result = image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for c in range(3):
                result[i][j][c] = saturate_cast(alpha * image[i][j][c] + beta, 0, 255)
    return result


def gamma_correction(image, gamma):
    result = image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for c in range(3):
                result[i][j][c] = saturate_cast(pow(image[i][j][c] / 255.0, gamma) * 255.0, 0, 255)
    return result


# def get_grayscale_from_rgb(image, weights=[1, 1, 1]):
#     result = np.ndarray(np.shape(image.shape[0:1]))
#     for i in range(image.shape[0]):
#         for j in range(image.shape[1]):
#             result[i][j] = saturate_cast(np.average(image[i][j], weights), 0, 255)
#     return result


def hist(image):
    """
    Function to calculate histogram of grayscaled image.
    Parameters:
        :image numpy array with 2 dims of int in range of 0:255
    Returns:
        result:array of numbers of pixels length of 255
    """
    result = np.zeros(256)

    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            result[image[i][j]] += 1

    return result
