import cv2
from lib import gamma_correction


# Load an color image in color
img = cv2.imread('data/3.jpg', 1)

# Show image
cv2.imshow('before', img)

# Apply correction
g = 0.5
img = gamma_correction(img, g)

# Show image
cv2.imshow('after', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
