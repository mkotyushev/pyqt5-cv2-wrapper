import cv2
from matplotlib import pyplot as plt


# Load an color image in color
img = cv2.imread('data/1.jpg', 1)
# Show image
cv2.imshow('image', img)

# Calculate histogram
hist_r = cv2.calcHist([img[:,:,0]], [0], None, [256], [0, 256])
hist_g = cv2.calcHist([img[:,:,1]], [0], None, [256], [0, 256])
hist_b = cv2.calcHist([img[:,:,2]], [0], None, [256], [0, 256])
plt.plot(range(256), hist_r, 'r',
         range(256), hist_g, 'g',
         range(256), hist_b, 'b')
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
