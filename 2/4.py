import cv2
import numpy as np
import copy
from lib import saturate_cast
from math import atan2, pi


def divide(img_1, img_2, dest_img):
    img1 = cv2.cvtColor(img_1, 7)
    img2 = cv2.cvtColor(img_2, 7)

    if img1.shape != img2.shape:
        return

    for i in range(img1.shape[0]):
        for j in range(img1.shape[1]):
            dest_img[i][j] = saturate_cast((atan2(img2[i][j], img1[i][j])) / pi * 2 * 255, 0, 255)


# Load an color image in color
img = cv2.imread('data/polosa.jpg', 1)
# Add noise
noise = np.zeros(img.shape, np.uint8)
# noise = cv2.cvtColor(noise, 7)
cv2.randn(noise, (20, 20, 20), (10, 10, 10))
cv2.imshow('noise', noise)
img += noise
# Show image
cv2.imshow('original', img)

# Blur
# Create copy and apply
filtered_img = copy.copy(img)
cv2.blur(img, (19, 19), filtered_img)
# Show image
cv2.imshow('blur', filtered_img)

# Median Blur
# Create copy and apply
filtered_img = copy.copy(img)
cv2.medianBlur(img, 19, filtered_img)
# Show image
cv2.imshow('median blur', filtered_img)

# Filter w custom kernel
# Create filter kernel
kernel = np.array([[0.1, 0.1, 0.2], [0.5, -1, 0.7], [0.1, 0.3, 0.2]])
# Create copy and apply
filtered_img = copy.copy(img)
cv2.filter2D(img, -1, kernel, filtered_img)
# Show image
cv2.imshow('custom mask', filtered_img)

# Sobel x
# TODO: explore parameters of Sobel
# Create copy and apply
filtered_img_1 = copy.copy(img)
cv2.Sobel(img, -1, 1, 0, filtered_img_1)
# Show image
cv2.imshow('sobel x', filtered_img_1)

# Sobel y
# TODO: explore parameters of Sobel
# Create copy and apply
filtered_img_2 = copy.copy(img)
cv2.Sobel(img, -1, 0, 1, filtered_img_2)
# Show image
cv2.imshow('sobel y', filtered_img_2)

# Gradient direction
divide(filtered_img_1, filtered_img_2, filtered_img)
cv2.imshow('sobel grad', filtered_img)

# Laplacian
# TODO: explore parameters of Laplacian
# Create copy and apply
filtered_img = copy.copy(img)
cv2.Laplacian(img, -1, filtered_img)
cv2.imshow('laplacian', filtered_img)

cv2.waitKey(0)
cv2.destroyAllWindows()
