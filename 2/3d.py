import cv2
import copy
from matplotlib import pyplot as plt


# Load an image in color
img = cv2.imread('data/3.jpg', 0)
# Show image
cv2.imshow('image', img)

# Calculate histogram
hist = cv2.calcHist([img], [0], None, [256], [0, 256])
plt.plot(range(256), hist, 'r', label="Before")

# Eq hist
eq_img = copy.copy(img)
cv2.equalizeHist(img, eq_img)
cv2.imshow('eq image', eq_img)

# Calculate eq histogram
eq_hist = cv2.calcHist([eq_img], [0], None, [256], [0, 256])
plt.plot(range(256), eq_hist, 'b', label="After")

plt.legend()
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
