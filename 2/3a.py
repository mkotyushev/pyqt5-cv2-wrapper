import cv2
from lib import linear_correction


# Load an color image in color
img = cv2.imread('data/3.jpg', 1)

# Show image
cv2.imshow('before', img)

# Apply correction
a = 0.25
b = 0.5
img = linear_correction(img, a, b)

# Show image
cv2.imshow('after', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
